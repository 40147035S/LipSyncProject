#ifndef __NEXTDATE_H__
#define __NEXTDATE_H__

#include <string>
#include "Range.h"

///< Class Date : Record certain date.
class Date{
public :
	static const myRange yearRange;
	static const myRange monthRange;
	static const myRange dayRange;

	static const std::string day_out_of_range;
	static const std::string month_out_of_range;
	static const std::string year_out_of_range;
public :
	///< Constructor
	Date() = default;
	///< Constructor with params 'yy' (year), 'mm' (month), 'dd' (day)
	Date(size_t yy, size_t mm, size_t dd)
		:year(yy), month(mm), day(dd) {}
	///< Copy Constructor
	Date(Date &other)
		:year(other.year), month(other.month), day(other.day) {}

	///< Destructor
	~Date() = default;

	///< Set up the Date with today's date.
	void today();
	///< Check if the Date is legal && if it's possible.
	std::string isLegalDate() const;

	///< Output the date according to the 3 attributes (yy/mm/dd).
	std::string OutputDate() const;

	///< Output the tomorrow date.
	std::string NextDate();
private :
	///< �~���
	size_t year, month, day;
};

#endif