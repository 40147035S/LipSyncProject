#include "TriangleTest.h"
#include <iostream>

TriangleTest::TriangleTest() {
	std::cout << "\t[TriangleTest] Constructor..." << std::endl;
}

TriangleTest:: ~TriangleTest() {
	std::cout << "\t[TriangleTest] Deconstructor..." << std::endl;
}

void TriangleTest::SetUp() {
	std::cout << "\t[TriangleTest] Set Up..." << std::endl;
}

void TriangleTest::TearDown() {
	std::cout << "\t[TriangleTest] Tear Down..." << std::endl;
}

TEST_F(TriangleTest, Test01_areTheEdgesLegal_WeakRobustEquivalenceClassTesting) {
	Triangle _t_WR1(-1, 5, 5);
	Triangle _t_WR2(5, -1, 5);
	Triangle _t_WR3(5, 5, -1);
	Triangle _t_WR4(201, 5, 5);
	Triangle _t_WR5(5, 201, 5);
	Triangle _t_WR6(5, 5, 201);

	EXPECT_EQ("Value of a is not in the range of 1..200", _t_WR1.isLegal());
	EXPECT_EQ("Value of b is not in the range of 1..200", _t_WR2.isLegal());
	EXPECT_EQ("Value of c is not in the range of 1..200", _t_WR3.isLegal());
	EXPECT_EQ("Value of a is not in the range of 1..200", _t_WR4.isLegal());
	EXPECT_EQ("Value of b is not in the range of 1..200", _t_WR5.isLegal());
	EXPECT_EQ("Value of c is not in the range of 1..200", _t_WR6.isLegal());
}

TEST_F(TriangleTest, Test02_areTheEdgesLegal_StrongRobustEquivalenceClassTesting) {
	Triangle _t_SR1(-1, 5, 5);
	Triangle _t_SR2(5, -1, 5);
	Triangle _t_SR3(5, 5, -1);
	Triangle _t_SR4(-1, -1, 5);
	Triangle _t_SR5(-1, 5, -1);
	Triangle _t_SR6(5, -1, -1);
	Triangle _t_SR7(-1, -1, -1);
	
	EXPECT_EQ("Value of a is not in the range of 1..200", _t_SR1.isLegal());
	EXPECT_EQ("Value of b is not in the range of 1..200", _t_SR2.isLegal());
	EXPECT_EQ("Value of c is not in the range of 1..200", _t_SR3.isLegal());
	EXPECT_EQ("Value of a,b are not in the range of 1..200", _t_SR4.isLegal());
	EXPECT_EQ("Value of a,c are not in the range of 1..200", _t_SR5.isLegal());
	EXPECT_EQ("Value of b,c are not in the range of 1..200", _t_SR6.isLegal());
	EXPECT_EQ("Value of a,b,c are not in the range of 1..200", _t_SR7.isLegal());
}

TEST_F(TriangleTest, Test03_theTypeOfTriangle_WeakNormalEquivalenceClassTesting) {
	Triangle _t_WN1(5, 5, 5);
	Triangle _t_WN2(2, 2, 3);
	Triangle _t_WN3(3, 4, 5);
	Triangle _t_WN4(4, 1, 2);

	EXPECT_EQ("Equilateral", _t_WN1.TriangleType());
	EXPECT_EQ("Isosceles", _t_WN2.TriangleType());
	EXPECT_EQ("Scalene", _t_WN3.TriangleType());
	EXPECT_EQ("Not A Triangle", _t_WN4.TriangleType());

}

TEST_F(TriangleTest, Test04_theTypeOfTriangle_BoundaryValueTesting) {
	//	min-
	Triangle _t_BV1(0, 5, 6);
	//	min
	Triangle _t_BV2(1, 5, 6);
	//	min+
	Triangle _t_BV3(2, 5, 6);
	//	max-
	Triangle _t_BV4(199, 5, 6);
	//	max
	Triangle _t_BV5(200, 5, 6);
	//	max+
	Triangle _t_BV6(201, 5, 6);

	EXPECT_EQ("Value of a is not in the range of 1..200", _t_BV1.TriangleType());
	EXPECT_EQ("Not A Triangle", _t_BV2.TriangleType());
	EXPECT_EQ("Scalene", _t_BV3.TriangleType());
	EXPECT_EQ("Not A Triangle", _t_BV4.TriangleType());
	EXPECT_EQ("Not A Triangle", _t_BV5.TriangleType());
	EXPECT_EQ("Value of a is not in the range of 1..200", _t_BV6.TriangleType());
}

TEST_F(TriangleTest, Test05_theTypeOfTriangle_BoundaryValueTesting) {
	//	min-
	Triangle _t_BV1(0, 5, 5);
	//	min
	Triangle _t_BV2(1, 5, 5);
	//	min+
	Triangle _t_BV3(2, 5, 5);
	//	max-
	Triangle _t_BV4(199, 5, 5);
	//	max
	Triangle _t_BV5(200, 5, 5);
	//	max+
	Triangle _t_BV6(201, 5, 5);

	EXPECT_EQ("Value of a is not in the range of 1..200", _t_BV1.TriangleType());
	EXPECT_EQ("Isosceles", _t_BV2.TriangleType());
	EXPECT_EQ("Isosceles", _t_BV3.TriangleType());
	EXPECT_EQ("Not A Triangle", _t_BV4.TriangleType());
	EXPECT_EQ("Not A Triangle", _t_BV5.TriangleType());
	EXPECT_EQ("Value of a is not in the range of 1..200", _t_BV6.TriangleType());
}

TEST_F(TriangleTest, Test06_theTypeOfTriangle_BoundaryValueTesting) {
	//	min-
	Triangle _t_BV1(0, 0, 5);
	//	min
	Triangle _t_BV2(1, 1, 5);
	//	min+
	Triangle _t_BV3(2, 2, 5);
	//	max-
	Triangle _t_BV4(199, 199, 5);
	//	max
	Triangle _t_BV5(200, 200, 5);
	//	max+
	Triangle _t_BV6(201, 201, 5);

	EXPECT_EQ("Value of a,b are not in the range of 1..200", _t_BV1.TriangleType());
	EXPECT_EQ("Not A Triangle", _t_BV2.TriangleType());
	EXPECT_EQ("Not A Triangle", _t_BV3.TriangleType());
	EXPECT_EQ("Isosceles", _t_BV4.TriangleType());
	EXPECT_EQ("Isosceles", _t_BV5.TriangleType());
	EXPECT_EQ("Value of a,b are not in the range of 1..200", _t_BV6.TriangleType());
}

TEST_F(TriangleTest, Test07_theTypeOfTriangle_BoundaryValueTesting) {
	//	min-
	Triangle _t_BV1(0, 0, 0);
	//	min
	Triangle _t_BV2(1, 1, 1);
	//	min+
	Triangle _t_BV3(2, 2, 2);
	//	max-
	Triangle _t_BV4(199, 199, 199);
	//	max
	Triangle _t_BV5(200, 200, 200);
	//	max+
	Triangle _t_BV6(201, 201, 201);

	EXPECT_EQ("Value of a,b,c are not in the range of 1..200", _t_BV1.TriangleType());
	EXPECT_EQ("Equilateral", _t_BV2.TriangleType());
	EXPECT_EQ("Equilateral", _t_BV3.TriangleType());
	EXPECT_EQ("Equilateral", _t_BV4.TriangleType());
	EXPECT_EQ("Equilateral", _t_BV5.TriangleType());
	EXPECT_EQ("Value of a,b,c are not in the range of 1..200", _t_BV6.TriangleType());
}

TEST_F(TriangleTest, Test08_Function_isLegal_PathTesting_Path01) {
	//	Test the function is Legal
	//	a, b, c : out of range

	Triangle _t_abc_out_of_range_case1(0, 0, 0);

	EXPECT_EQ("Value of a,b,c are not in the range of 1..200", _t_abc_out_of_range_case1.isLegal());

	Triangle _t_abc_out_of_range_case2(-100, -100, -100);

	EXPECT_EQ("Value of a,b,c are not in the range of 1..200", _t_abc_out_of_range_case2.isLegal());

	Triangle _t_abc_out_of_range_case3(201, 201, 201);

	EXPECT_EQ("Value of a,b,c are not in the range of 1..200", _t_abc_out_of_range_case3.isLegal());

}

TEST_F(TriangleTest, Test09_Function_isLegal_PathTesting_Path02) {
	//	Test the function is Legal
	//	a, b : out of range
	//	c : in range

	Triangle _t_ab_out_of_range_case1(0, 0, 100);

	EXPECT_EQ("Value of a,b are not in the range of 1..200", _t_ab_out_of_range_case1.isLegal());

	Triangle _t_ab_out_of_range_case2(-100, -100, 100);

	EXPECT_EQ("Value of a,b are not in the range of 1..200", _t_ab_out_of_range_case2.isLegal());

	Triangle _t_ab_out_of_range_case3(201, 201, 100);

	EXPECT_EQ("Value of a,b are not in the range of 1..200", _t_ab_out_of_range_case3.isLegal());
}

TEST_F(TriangleTest, Test10_Function_isLegal_PathTesting_Path03) {
	//	Test the function is Legal
	//	a, c : out of range
	//	b : in range

	Triangle _t_ac_out_of_range_case1(0, 100, 0);

	EXPECT_EQ("Value of a,c are not in the range of 1..200", _t_ac_out_of_range_case1.isLegal());

	Triangle _t_ac_out_of_range_case2(-100, 100, -100);

	EXPECT_EQ("Value of a,c are not in the range of 1..200", _t_ac_out_of_range_case2.isLegal());

	Triangle _t_ac_out_of_range_case3(201, 100, 201);

	EXPECT_EQ("Value of a,c are not in the range of 1..200", _t_ac_out_of_range_case3.isLegal());
}

TEST_F(TriangleTest, Test11_Function_isLegal_PathTesting_Path04) {
	//	Test the function is Legal
	//	a : out of range
	//	b, c : in range

	Triangle _t_a_out_of_range_case1(0, 100, 100);

	EXPECT_EQ("Value of a is not in the range of 1..200", _t_a_out_of_range_case1.isLegal());

	Triangle _t_a_out_of_range_case2(-100, 100, 100);

	EXPECT_EQ("Value of a is not in the range of 1..200", _t_a_out_of_range_case2.isLegal());

	Triangle _t_a_out_of_range_case3(201, 200, 200);

	EXPECT_EQ("Value of a is not in the range of 1..200", _t_a_out_of_range_case3.isLegal());
}

TEST_F(TriangleTest, Test12_Function_isLegal_PathTesting_Path05) {
	//	Test the function is Legal
	//	b, c : out of range
	//	a : in range

	Triangle _t_bc_out_of_range_case1(100, 0, 0);

	EXPECT_EQ("Value of b,c are not in the range of 1..200", _t_bc_out_of_range_case1.isLegal());

	Triangle _t_bc_out_of_range_case2(100, -100, -100);

	EXPECT_EQ("Value of b,c are not in the range of 1..200", _t_bc_out_of_range_case2.isLegal());

	Triangle _t_bc_out_of_range_case3(200, 201, 201);

	EXPECT_EQ("Value of b,c are not in the range of 1..200", _t_bc_out_of_range_case3.isLegal());
}

TEST_F(TriangleTest, Test13_Function_isLegal_PathTesting_Path06) {
	//	Test the function is Legal
	//	b : out of range
	//	a, c : in range

	Triangle _t_b_out_of_range_case1(100, 0, 100);

	EXPECT_EQ("Value of b is not in the range of 1..200", _t_b_out_of_range_case1.isLegal());

	Triangle _t_b_out_of_range_case2(100, -100, 100);

	EXPECT_EQ("Value of b is not in the range of 1..200", _t_b_out_of_range_case2.isLegal());

	Triangle _t_b_out_of_range_case3(200, 201, 200);

	EXPECT_EQ("Value of b is not in the range of 1..200", _t_b_out_of_range_case3.isLegal());
}

TEST_F(TriangleTest, Test14_Function_isLegal_PathTesting_Path07) {
	//	Test the function is Legal
	//	c : out of range
	//	a, b : in range

	Triangle _t_c_out_of_range_case1(100, 100, 0);

	EXPECT_EQ("Value of c is not in the range of 1..200", _t_c_out_of_range_case1.isLegal());

	Triangle _t_c_out_of_range_case2(100, 100, -100);

	EXPECT_EQ("Value of c is not in the range of 1..200", _t_c_out_of_range_case2.isLegal());

	Triangle _t_c_out_of_range_case3(200, 200, 201);

	EXPECT_EQ("Value of c is not in the range of 1..200", _t_c_out_of_range_case3.isLegal());
}

TEST_F(TriangleTest, Test15_Function_isLegal_PathTesting_Path08) {
	//	Test the function is Legal
	//	(none) : out of range
	//	a, b, c : in range
	//	NOT EQUAL TO 'IS TRIANGLE'

	Triangle _t_none_out_of_range_case1(1, 200, 199);

	EXPECT_EQ("", _t_none_out_of_range_case1.isLegal());

	Triangle _t_none_out_of_range_case2(101, 200, 200);

	EXPECT_EQ("", _t_none_out_of_range_case2.isLegal());

	Triangle _t_none_out_of_range_case3(200, 200, 200);

	EXPECT_EQ("", _t_none_out_of_range_case3.isLegal());
}

TEST_F(TriangleTest, Test16_Function_TriangleType_PathTesting_Path01) {
	//	Status : Edge Not Legal	

	Triangle _t_edge_not_legal_case1(0, 0, 0);

	EXPECT_EQ("Value of a,b,c are not in the range of 1..200", _t_edge_not_legal_case1.TriangleType());

	Triangle _t_edge_not_legal_case2(101, 201, 201);

	EXPECT_EQ("Value of b,c are not in the range of 1..200", _t_edge_not_legal_case2.TriangleType());

	Triangle _t_edge_not_legal_case3(201, 200, 201);

	EXPECT_EQ("Value of a,c are not in the range of 1..200", _t_edge_not_legal_case3.TriangleType());
}

TEST_F(TriangleTest, Test17_Function_TriangleType_PathTesting_Path02) {
	//	Status : Not A Triangle (in the premise that no illegal edge)

	//	a + c > b 不成立
	Triangle _t_not_a_triangle_case1(1, 200, 199);

	EXPECT_EQ("Not A Triangle", _t_not_a_triangle_case1.TriangleType());

	//	a + b > c 不成立
	Triangle _t_not_a_triangle_case2(20, 170, 200);

	EXPECT_EQ("Not A Triangle", _t_not_a_triangle_case2.TriangleType());

	//	b + c > a 不成立
	Triangle _t_not_a_triangle_case3(200, 100, 100);

	EXPECT_EQ("Not A Triangle", _t_not_a_triangle_case3.TriangleType());
}

TEST_F(TriangleTest, Test18_Function_TriangleType_PathTesting_Path03) {
	//	Status : Equilateral
	//	a == b && a == c 

	Triangle _t_equilateral_case1(100, 100, 100);

	EXPECT_EQ("Equilateral", _t_equilateral_case1.TriangleType());

	Triangle _t_equilateral_case2(200, 200, 200);

	EXPECT_EQ("Equilateral", _t_equilateral_case2.TriangleType());

	Triangle _t_equilateral_case3(20, 20, 20);

	EXPECT_EQ("Equilateral", _t_equilateral_case3.TriangleType());
}

TEST_F(TriangleTest, Test19_Function_TriangleType_PathTesting_Path04) {
	//	Status : Scalene
	//	a != b && a != c && b != c

	Triangle _t_scalene_case1(10, 200, 199);

	EXPECT_EQ("Scalene", _t_scalene_case1.TriangleType());

	Triangle _t_scalene_case2(101, 200, 102);

	EXPECT_EQ("Scalene", _t_scalene_case2.TriangleType());

	Triangle _t_scalene_case3(3, 4, 5);

	EXPECT_EQ("Scalene", _t_scalene_case3.TriangleType());
}

TEST_F(TriangleTest, Test20_Function_TriangleType_PathTesting_Path05) {
	//	Status : Isosceles
	//	a == b || a == c || b == c && !(a==b==c)

	Triangle _t_isosceles_case1(100, 100, 199);

	EXPECT_EQ("Isosceles", _t_isosceles_case1.TriangleType());

	Triangle _t_isosceles_case2(5, 5, 2);

	EXPECT_EQ("Isosceles", _t_isosceles_case2.TriangleType());

	Triangle _t_isosceles_case3(200, 200, 5);

	EXPECT_EQ("Isosceles", _t_isosceles_case3.TriangleType());
}






