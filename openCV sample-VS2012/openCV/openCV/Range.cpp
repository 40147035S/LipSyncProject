#include "Range.h"
#include <sstream>

myRange::myRange(int lower, int upper) {
	if (lower > upper)
		throw std::out_of_range("Lower must < upper.");

	floor = lower;
	ceiling = upper;
}

bool myRange::inRange(int number) const{
	return (number >= floor && number <= ceiling);
}

std::string myRange::ShowRange() const {
	std::stringstream sFloor, sCeiling;

	sFloor << floor;
	sCeiling << ceiling;

	return sFloor.str() + ".." + sCeiling.str();
}