#include "ComissionTest.h"
#include <iostream>

ComissionTest::ComissionTest() {
	std::cout << "\t[ComissionTest] Constructor..." << std::endl;
}

ComissionTest::~ComissionTest() {
	std::cout << "\t[ComissionTest] Deconstructor..." << std::endl;
}

void ComissionTest::SetUp() {
	std::cout << "\t[ComissionTest] Set Up..." << std::endl;
}

void ComissionTest::TearDown() {
	std::cout << "\t[ComissionTest] Tear Down..." << std::endl;
}

TEST_F(ComissionTest, Test01_) {
	Comission _c1;

	//	insufficient amount of sold product.
	EXPECT_THROW(_c1.soldProduct(-1, 0, 0), std::exception);

	EXPECT_EQ(_c1.soldProduct(1, 0, 0), "Locks sold:1\nStocks sold:0\nBarrels sold:0\n");
	//	insufficient amount of sold product.
	EXPECT_THROW(_c1.soldProduct(-1, 0, 0), std::exception);
	
	EXPECT_EQ(_c1.soldProduct(0, 1, 0), "Locks sold:1\nStocks sold:1\nBarrels sold:0\n");
	//	insufficient amount of sold product.
	EXPECT_THROW(_c1.soldProduct(-1, 0, 0), std::exception);

	EXPECT_EQ(_c1.soldProduct(0, 0, 1), "Locks sold:1\nStocks sold:1\nBarrels sold:1\n");
	//	insufficient amount of sold product.
	EXPECT_NO_THROW(_c1.soldProduct(-1, 0, 0));
}

TEST_F(ComissionTest, Test02_) {
	Comission _c2;

	EXPECT_EQ(_c2.soldProduct(1, 1, 1), "Locks sold:1\nStocks sold:1\nBarrels sold:1\n");
	//	45 * 1 + 30 * 1 + 25 * 1 = $100 (Product Profit)
	//	$100 (< $1000) : $100 * 10% = $10
	EXPECT_EQ(_c2.soldProduct(-1, 2, 0), "Comission is 10\n");

	// min-
	EXPECT_EQ(_c2.soldProduct(-2, 1, 1), "Value of lock is not natural number.\n");
	EXPECT_EQ(_c2.soldProduct(1, -1, 1), "Value of stock is not natural number.\n");
	EXPECT_EQ(_c2.soldProduct( 1, 1,-1), "Value of barrel is not natural number.\n");
	EXPECT_EQ(_c2.soldProduct(-2,-1, 1), "Value of lock is not natural number.\nValue of stock is not natural number.\n");
	EXPECT_EQ(_c2.soldProduct(-2, 1,-1), "Value of lock is not natural number.\nValue of barrel is not natural number.\n");
	EXPECT_EQ(_c2.soldProduct( 1,-1,-1), "Value of stock is not natural number.\nValue of barrel is not natural number.\n");
	EXPECT_EQ(_c2.soldProduct(-2,-1,-1), "Value of lock is not natural number.\nValue of stock is not natural number.\nValue of barrel is not natural number.\n");

	//	45 * 1 + 30 * 1 + 25 * 1 = $100 (Product Profit)
	//	$100 (< $1000) : $100 * 10% = $10
	EXPECT_EQ(_c2.soldProduct(-1, 2, 0), "Comission is 10\n");
}

TEST_F(ComissionTest, Test03_) {
	Comission _WR1;

	EXPECT_EQ(_WR1.soldProduct(1, 2, 3), "Locks sold:1\nStocks sold:2\nBarrels sold:3\n");
	//	45 * 1 + 30 * 2 + 25 * 3 = $180 (Product Profit)
	//	$180 (< $1000) : $180 * 10% = $18
	EXPECT_EQ(_WR1.soldProduct(-1, 0, 0), "Comission is 18\n");
	EXPECT_EQ(_WR1.soldProduct(3, 2, 1), "Locks sold:4\nStocks sold:4\nBarrels sold:4\n");
	//	45 * 4 + 30 * 4 + 25 * 4 = $400 (Product Profit)
	//	$400 (< $1000) : $400 * 10% = $40
	EXPECT_EQ(_WR1.soldProduct(-1, 0, 0), "Comission is 40\n");
}

TEST_F(ComissionTest, Test04_) {
	Comission _c4;

	//	max+
	EXPECT_EQ(_c4.soldProduct(71, 1, 1), "The storage of locks are not enough\n");
	EXPECT_EQ(_c4.soldProduct( 1,81, 1), "The storage of stocks are not enough\n");
	EXPECT_EQ(_c4.soldProduct( 1, 1,91), "The storage of barrels are not enough\n");
	EXPECT_EQ(_c4.soldProduct(71,81, 1), "The storage of locks are not enough\nThe storage of stocks are not enough\n");
	EXPECT_EQ(_c4.soldProduct(71, 1,91), "The storage of locks are not enough\nThe storage of barrels are not enough\n");
	EXPECT_EQ(_c4.soldProduct( 1,81,91), "The storage of stocks are not enough\nThe storage of barrels are not enough\n");
	EXPECT_EQ(_c4.soldProduct(71,81,91), "The storage of locks are not enough\nThe storage of stocks are not enough\nThe storage of barrels are not enough\n");

	//	insufficient amount of sold product.
	EXPECT_THROW(_c4.soldProduct(-1, 0, 0), std::exception);

	//	max
	EXPECT_EQ(_c4.soldProduct(70, 0, 0), "Locks sold:70\nStocks sold:0\nBarrels sold:0\n");
	EXPECT_EQ(_c4.soldProduct( 0,80, 0), "Locks sold:70\nStocks sold:80\nBarrels sold:0\n");
	EXPECT_EQ(_c4.soldProduct( 0, 0,90), "Locks sold:70\nStocks sold:80\nBarrels sold:90\n");
	//	45 * 70 + 30 * 80 + 25 * 90 = $7800 (Product Profit)
	//	$7800 (> $1800) : $1000 * 10% + $800 * 15% + $6000 * 20% = $1420
	EXPECT_EQ(_c4.soldProduct(-1, 0, 0), "Comission is 1420\n");
}

TEST_F(ComissionTest, Test05_) {
	Comission _c5;

	EXPECT_EQ(_c5.soldProduct(1, 0, 0), "Locks sold:1\nStocks sold:0\nBarrels sold:0\n");
	EXPECT_EQ(_c5.soldProduct(0, 1, 0), "Locks sold:1\nStocks sold:1\nBarrels sold:0\n");
	EXPECT_EQ(_c5.soldProduct(0, 0, 1), "Locks sold:1\nStocks sold:1\nBarrels sold:1\n");

	EXPECT_EQ(_c5.soldProduct(69, 79, 89), "Locks sold:70\nStocks sold:80\nBarrels sold:90\n");
	
	EXPECT_EQ(_c5.soldProduct(1, 0, 0), "The storage of locks are not enough\n");
	EXPECT_EQ(_c5.soldProduct(0, 1, 0), "The storage of stocks are not enough\n");
	EXPECT_EQ(_c5.soldProduct(0, 0, 1), "The storage of barrels are not enough\n");

}

TEST_F(ComissionTest, Test06_) {
	Comission _c6;

	EXPECT_EQ(_c6.soldProduct(1, 1, 36), "Locks sold:1\nStocks sold:1\nBarrels sold:36\n");
	//	45 * 1 + 30 * 1 + 25 * 36 = $975 (Product Profit)
	//	$975 (<= $1000) : $975 * 10% = $97.5
	EXPECT_EQ(_c6.soldProduct(-1, 0, 0), "Comission is 97.5\n");

	EXPECT_EQ(_c6.soldProduct(0, 0, 1), "Locks sold:1\nStocks sold:1\nBarrels sold:37\n");
	//	45 * 1 + 30 * 1 + 25 * 37 = $1000 (Product Profit)
	//	$1000 (<= $1000) : $1000 * 10% = $100
	EXPECT_EQ(_c6.soldProduct(-1, 0, 0), "Comission is 100\n");

	EXPECT_EQ(_c6.soldProduct(0, 1, 0), "Locks sold:1\nStocks sold:2\nBarrels sold:37\n");
	//	45 * 1 + 30 * 2 + 25 * 37 = $1030 (Product Profit)
	//	$1030 (> $1000) : $1000 * 10% + $30 * 15% = $100
	EXPECT_EQ(_c6.soldProduct(-1, 0, 0), "Comission is 104.5\n");

	EXPECT_EQ(_c6.soldProduct(0, 9, 19), "Locks sold:1\nStocks sold:11\nBarrels sold:56\n");
	//	45 * 1 + 30 * 11 + 25 * 56 = $1775 (Product Profit)
	//	$1775 (> $1000) : $1000 * 10% + $775 * 15% = $216.25
	EXPECT_EQ(_c6.soldProduct(-1, 0, 0), "Comission is 216.25\n");

	EXPECT_EQ(_c6.soldProduct(0, 0, 1), "Locks sold:1\nStocks sold:11\nBarrels sold:57\n");
	//	45 * 1 + 30 * 11 + 25 * 57 = $1800 (Product Profit)
	//	$1800 (> $1000) : $1000 * 10% + $800 * 15% = $220
	EXPECT_EQ(_c6.soldProduct(-1, 0, 0), "Comission is 220\n");

	EXPECT_EQ(_c6.soldProduct(1, 0, 0), "Locks sold:2\nStocks sold:11\nBarrels sold:57\n");
	//	45 * 2 + 30 * 11 + 25 * 57 = $1800 (Product Profit)
	//	$1845 (> $1000) : $1000 * 10% + $800 * 15% + $45 * 20% = $229
	EXPECT_EQ(_c6.soldProduct(-1, 0, 0), "Comission is 229\n");
}

//	Path 1 : 3 - 4
//	Exception : 'Insufficient Amount of Products'
//	Status : Sold Nothing.
TEST_F(ComissionTest, Test07_Function_ComissionValue_PathTesting_Path1) {
	Comission _t_InsufficientAmountOfProduct;

	EXPECT_THROW(_t_InsufficientAmountOfProduct.ComissionValue(), std::exception );
}

//	Path 2 : 3 - 8 - 9
//	Status : Product Value <= 1000
TEST_F(ComissionTest, Test08_Function_ComissionValue_PathTesting_Path2) {
	Comission _t_LessThan1000;

	//	sold : 1 lock, 1 stock, 1 barrel
	//	1 * 45 +  1 * 30 + 1 * 25 = 100
	//	0 + 100 = 100 ( < 1000 => 10 )
	_t_LessThan1000.soldProduct(1, 1, 1);
	EXPECT_EQ(10, _t_LessThan1000.ComissionValue());
	//	sold : 2 locks
	//	2 * 45 = 90
	//	100 + 90 = 190 ( < 1000 => 19 )
	_t_LessThan1000.soldProduct(2, 0, 0);
	EXPECT_EQ(19, _t_LessThan1000.ComissionValue());
	//	sold : 20 stocks
	//	20 * 30 = 600
	//	190 + 600 = 790 ( < 1000 => 79 )
	_t_LessThan1000.soldProduct(0, 20, 0);
	EXPECT_EQ(79, _t_LessThan1000.ComissionValue());
	//	sold : 8 barrels
	//	8 * 25 = 200
	//	790 + 200 = 990 ( < 1000 => 99 )
	_t_LessThan1000.soldProduct(0, 0, 8);
	EXPECT_EQ(99, _t_LessThan1000.ComissionValue());
}

//	Path 3 : 3 - 8 - 10 - 11
//	Status : 1000 < Product Value <= 1800
TEST_F(ComissionTest, Test09_Function_ComissionValue_PathTesting_Path3) {
	Comission _t_LessThan1800;

	//	sold : 40 stocks
	//	40 * 30 = 1200
	//	0 + 1200 = 1200 ( 200 * 0.15 + 1000 * 0.1 = 130  )
	_t_LessThan1800.soldProduct(0, 40, 0);
	
	//	sold : 10 locks
	//	10 * 45 = 450
	//	1200 + 450 = 1650 ( 650 * 0.15 + 1000 * 0.1 = 197.5 )
	_t_LessThan1800.soldProduct(10, 0, 0);
	
	//	sold : 6 barrels
	//	6 * 25 = 150
	//	1650 + 150 = 1800 ( 800 * 0.15 + 1000 * 0.1 = 220 )
	_t_LessThan1800.soldProduct(0, 0, 6);
	EXPECT_EQ(220, _t_LessThan1800.ComissionValue());
}

//	Path 4 : 3 - 8 - 10 - 12 - 13
//	Status : 1800 < Product Value
TEST_F(ComissionTest, Test10_Function_ComissionValue_PathTesting_Path4) {
	Comission _t_MoreThan1800;

	//	sold : 40 locks, 1 stock, 1 barrel
	//	40 * 45 + 1 * 30 + 1 * 25 = 1855
	//	1855 ( 55 * 0.2 + 800 * 0.15 + 1000 * 0.1 = 231 )
	_t_MoreThan1800.soldProduct(40, 1, 1);
	EXPECT_EQ( 231, _t_MoreThan1800.ComissionValue() );
	//	sold : 29 stocks, 29 barrels
	//	40 * 45 + 30 * 30 + 30 * 25 = 3450
	//	3450 ( 1650 * 0.2 + 800 * 0.15 + 1000 * 0.1 = 550  )
	_t_MoreThan1800.soldProduct(0, 29, 29);
	EXPECT_EQ(550, _t_MoreThan1800.ComissionValue());
}

//	Path 1 : 4 - 5
//	Status : Output Comission Value
TEST_F(ComissionTest, Test11_Function_SoldProduct_PathTesting_Path1) {
	Comission _t_balance;

	//	1 lock, 1 stock, 1 barrel
	//	45 + 30 + 25 = 100
	//	0 + 100 = 100
	_t_balance.soldProduct(1, 1, 1);
	EXPECT_EQ("Comission is 10\n", _t_balance.soldProduct(-1, 0, 0));
	//	1 lock, 1 stock, 1 barrel
	//	45 + 30 + 25 = 100	
	//	100 + 100 = 200
	_t_balance.soldProduct(1, 1, 1);
	EXPECT_EQ("Comission is 20\n", _t_balance.soldProduct(-1, 0, 0));
}

//	Path 2 : 4 - 12 - 13 - 14 - 16 - 19 
//	Status : Lock Value < 0
TEST_F(ComissionTest, Test12_Function_SoldProduct_PathTesting_Path2) {
	Comission _t_LocksValueLessThanZero;

	_t_LocksValueLessThanZero.soldProduct(1, 1, 1);
	EXPECT_EQ("Value of lock is not natural number.\n", _t_LocksValueLessThanZero.soldProduct(-2, 1, 2) );
	EXPECT_EQ("Value of lock is not natural number.\n", _t_LocksValueLessThanZero.soldProduct(-3, 2, 10) );
	EXPECT_EQ("Value of lock is not natural number.\n", _t_LocksValueLessThanZero.soldProduct(-5, 7, 3) );
}

//	Path 3 : 4 - 12 - 13 - 14 - 16 - 17 - 19
//	Status : Lock Value < 0, Barrel Value < 0
TEST_F(ComissionTest, Test13_Function_SoldProduct_PathTesting_Path3) {
	Comission _t_LocksBarrelsValueLessThanZero;

	_t_LocksBarrelsValueLessThanZero.soldProduct(1, 1, 1);
	EXPECT_EQ("Value of lock is not natural number.\nValue of barrel is not natural number.\n", _t_LocksBarrelsValueLessThanZero.soldProduct(-2, 10, -5));
	EXPECT_EQ("Value of lock is not natural number.\nValue of barrel is not natural number.\n", _t_LocksBarrelsValueLessThanZero.soldProduct(-5, 2, -1));
	EXPECT_EQ("Value of lock is not natural number.\nValue of barrel is not natural number.\n", _t_LocksBarrelsValueLessThanZero.soldProduct(-6, 0, -2));
}

//	Path 4 : 4 - 12 - 13 - 14 - 15 - 16 - 19
//	Status : Lock Value < 0, Stock Value < 0
TEST_F(ComissionTest, Test14_Function_SoldProduct_PathTesting_Path4) {
	Comission _t_LocksStocksValueLessThanZero;

	_t_LocksStocksValueLessThanZero.soldProduct(1, 1, 1);
	EXPECT_EQ("Value of lock is not natural number.\nValue of stock is not natural number.\n", _t_LocksStocksValueLessThanZero.soldProduct(-2, -10, 5));
	EXPECT_EQ("Value of lock is not natural number.\nValue of stock is not natural number.\n", _t_LocksStocksValueLessThanZero.soldProduct(-5, -12, 1));
	EXPECT_EQ("Value of lock is not natural number.\nValue of stock is not natural number.\n", _t_LocksStocksValueLessThanZero.soldProduct(-6, -2, 2));
}

//	Path 5 : 4 - 12 - 13 - 14 - 15 - 16 - 17 - 19
//	Status : Lock Value < 0, Stock Value < 0, Barrel Value < 0
TEST_F(ComissionTest, Test15_Function_SoldProduct_PathTesting_Path5) {
	Comission _t_LocksStocksBarrelsValueLessThanZero;

	_t_LocksStocksBarrelsValueLessThanZero.soldProduct(1, 1, 1);
	EXPECT_EQ("Value of lock is not natural number.\nValue of stock is not natural number.\nValue of barrel is not natural number.\n", _t_LocksStocksBarrelsValueLessThanZero.soldProduct(-2, -3, - 4));
	EXPECT_EQ("Value of lock is not natural number.\nValue of stock is not natural number.\nValue of barrel is not natural number.\n", _t_LocksStocksBarrelsValueLessThanZero.soldProduct(-12, -13, -14));
	EXPECT_EQ("Value of lock is not natural number.\nValue of stock is not natural number.\nValue of barrel is not natural number.\n", _t_LocksStocksBarrelsValueLessThanZero.soldProduct(-20, -30, -40));
}

//	Path 6 : 4 - 12 - 14 - 16 - 17 - 19
//	Status : Barrel Value < 0
TEST_F(ComissionTest, Test16_Function_SoldProduct_PathTesting_Path6) {
	Comission _t_BarrelsValueLessThanZero;

	_t_BarrelsValueLessThanZero.soldProduct(1, 1, 1);
	EXPECT_EQ("Value of barrel is not natural number.\n", _t_BarrelsValueLessThanZero.soldProduct(2, 3, -5) );
	EXPECT_EQ("Value of barrel is not natural number.\n", _t_BarrelsValueLessThanZero.soldProduct(12, 13, -50));
	EXPECT_EQ("Value of barrel is not natural number.\n", _t_BarrelsValueLessThanZero.soldProduct(20, 30, -1));
}

//	Path 7 : 4 - 12 - 14 - 15 - 16 - 19
//  Status : Stock Value < 0
TEST_F(ComissionTest, Test17_Function_SoldProduct_PathTesting_Path7) {
	Comission _t_StocksValueLessThanZero;

	_t_StocksValueLessThanZero.soldProduct(1, 1, 1);
	EXPECT_EQ("Value of lock is not natural number.\n", _t_StocksValueLessThanZero.soldProduct(-2, 3, 5));
	EXPECT_EQ("Value of lock is not natural number.\n", _t_StocksValueLessThanZero.soldProduct(-12, 13, 50));
	EXPECT_EQ("Value of lock is not natural number.\n", _t_StocksValueLessThanZero.soldProduct(-20, 30, 1));
}

//	Path 8 : 4 - 12 - 14 - 15 - 16 - 17 - 19
//	Status : Stock Value < 0, Barrel Value < 0
TEST_F(ComissionTest, Test18_Function_SoldProduct_PathTesting_Path8) {
	Comission _t_StocksBarrelsValueLessThanZero;

	_t_StocksBarrelsValueLessThanZero.soldProduct(1, 1, 1);
	EXPECT_EQ("Value of stock is not natural number.\nValue of barrel is not natural number.\n", _t_StocksBarrelsValueLessThanZero.soldProduct(2, -3, -5));
	EXPECT_EQ("Value of stock is not natural number.\nValue of barrel is not natural number.\n", _t_StocksBarrelsValueLessThanZero.soldProduct(12, -13, -50));
	EXPECT_EQ("Value of stock is not natural number.\nValue of barrel is not natural number.\n", _t_StocksBarrelsValueLessThanZero.soldProduct(20, -30, -1));
}

//	#2

//	Path 1 : 19 - 23 - 26 - 29 - 32 - 44
//	Status : None of the lock, stock, barrel 's storage is not enough.
TEST_F(ComissionTest, Test19_Function_SoldProduct_PathTesting_Path1) {
	Comission _t_NoneOfLockStockBarrelNotEnough;
	
	EXPECT_EQ("Locks sold:1\nStocks sold:1\nBarrels sold:1\n", _t_NoneOfLockStockBarrelNotEnough.soldProduct(1, 1, 1) );
	EXPECT_EQ("Locks sold:3\nStocks sold:1\nBarrels sold:1\n", _t_NoneOfLockStockBarrelNotEnough.soldProduct(2, 0, 0) );
	EXPECT_EQ("Locks sold:3\nStocks sold:3\nBarrels sold:1\n", _t_NoneOfLockStockBarrelNotEnough.soldProduct(0, 2, 0) );
	EXPECT_EQ("Locks sold:3\nStocks sold:3\nBarrels sold:3\n", _t_NoneOfLockStockBarrelNotEnough.soldProduct(0, 0, 2) );
	EXPECT_EQ("Locks sold:6\nStocks sold:6\nBarrels sold:3\n", _t_NoneOfLockStockBarrelNotEnough.soldProduct(3, 3, 0) );
	EXPECT_EQ("Locks sold:9\nStocks sold:6\nBarrels sold:6\n", _t_NoneOfLockStockBarrelNotEnough.soldProduct(3, 0, 3) );
	EXPECT_EQ("Locks sold:9\nStocks sold:9\nBarrels sold:9\n", _t_NoneOfLockStockBarrelNotEnough.soldProduct(0, 3, 3) );
	EXPECT_EQ("Locks sold:13\nStocks sold:13\nBarrels sold:13\n", _t_NoneOfLockStockBarrelNotEnough.soldProduct(4, 4, 4));
}
//	Path 2 : 19 - 23 - 26 - 29 - 30 - 32 - 33
//	Status : Barreal's storage is not enough.
TEST_F(ComissionTest, Test20_Function_SoldProduct_PathTesting_Path2) {
	Comission _t_BarrelStorageNotEnough;
	
	EXPECT_EQ("The storage of barrels are not enough\n", _t_BarrelStorageNotEnough.soldProduct(1, 1, 91));
	EXPECT_EQ("Locks sold:1\nStocks sold:1\nBarrels sold:90\n", _t_BarrelStorageNotEnough.soldProduct(1, 1, 90));
	EXPECT_EQ("The storage of barrels are not enough\n", _t_BarrelStorageNotEnough.soldProduct(0, 0, 2));
	EXPECT_EQ("The storage of barrels are not enough\n", _t_BarrelStorageNotEnough.soldProduct(0, 1, 20));
}
//	Path 3 : 19 - 23 - 26 - 27 - 29 - 32 - 33
//	Status : Stock's storage is not enough.
TEST_F(ComissionTest, Test21_Function_SoldProduct_PathTesting_Path3) {
	Comission _t_StockStorageNotEnough;

	EXPECT_EQ("The storage of stocks are not enough\n", _t_StockStorageNotEnough.soldProduct(1, 81, 2) );
	EXPECT_EQ("Locks sold:1\nStocks sold:80\nBarrels sold:2\n", _t_StockStorageNotEnough.soldProduct(1, 80, 2));
	EXPECT_EQ("The storage of stocks are not enough\n", _t_StockStorageNotEnough.soldProduct(0, 10, 2));
	EXPECT_EQ("The storage of stocks are not enough\n", _t_StockStorageNotEnough.soldProduct(1, 2, 2));
}
//	Path 4 : 19 - 23 - 26 - 27 - 29 - 30 - 32 - 33
//	Status : Stock, Barrel's storage is not enough.
TEST_F(ComissionTest, Test22_Function_SoldProduct_PathTesting_Path4) {
	Comission _t_StockBarrelStorageNotEnough;

	EXPECT_EQ("The storage of stocks are not enough\nThe storage of barrels are not enough\n", _t_StockBarrelStorageNotEnough.soldProduct(1, 81, 91));
	EXPECT_EQ("Locks sold:11\nStocks sold:80\nBarrels sold:90\n", _t_StockBarrelStorageNotEnough.soldProduct(11, 80, 90));
	EXPECT_EQ("The storage of stocks are not enough\nThe storage of barrels are not enough\n", _t_StockBarrelStorageNotEnough.soldProduct(5, 8, 9));
	EXPECT_EQ("The storage of stocks are not enough\nThe storage of barrels are not enough\n", _t_StockBarrelStorageNotEnough.soldProduct(5, 18, 19));
}
//	Path 5 : 19 - 23 - 24 - 26 - 29 - 32 - 33
//	Status : Lock's storage is not enough.
TEST_F(ComissionTest, Test23_Function_SoldProduct_PathTesting_Path5) {
	Comission _t_LockStorageNotEnough;

	EXPECT_EQ("The storage of locks are not enough\n", _t_LockStorageNotEnough.soldProduct(71, 1, 1));
	EXPECT_EQ("Locks sold:70\nStocks sold:2\nBarrels sold:1\n", _t_LockStorageNotEnough.soldProduct(70, 2, 1));
	EXPECT_EQ("The storage of locks are not enough\n", _t_LockStorageNotEnough.soldProduct(3, 1, 1));
	EXPECT_EQ("The storage of locks are not enough\n", _t_LockStorageNotEnough.soldProduct(32, 10, 1));
}
//	Path 6 : 19 - 23 - 24 - 26 - 29 - 30 - 32 - 33
//	Status : Lock, Barrel's storage is not enough.
TEST_F(ComissionTest, Test24_Function_SoldProduct_PathTesting_Path6) {
	Comission _t_LockBarrelStorageNotEnough;

	EXPECT_EQ("The storage of locks are not enough\nThe storage of barrels are not enough\n", _t_LockBarrelStorageNotEnough.soldProduct(71, 1, 91));
	EXPECT_EQ("Locks sold:70\nStocks sold:2\nBarrels sold:90\n", _t_LockBarrelStorageNotEnough.soldProduct(70, 2, 90));
	EXPECT_EQ("The storage of locks are not enough\nThe storage of barrels are not enough\n", _t_LockBarrelStorageNotEnough.soldProduct(3, 1, 1));
	EXPECT_EQ("The storage of locks are not enough\nThe storage of barrels are not enough\n", _t_LockBarrelStorageNotEnough.soldProduct(32, 10, 1));
}
//	Path 7 : 19 - 23 - 24 - 26 - 27 - 29 - 32 - 33
//	Status : Lock, Stock's storage is not enough.
TEST_F(ComissionTest, Test25_Function_SoldProduct_PathTesting_Path7) {
	Comission _t_LockSockStorageNotEnough;

	EXPECT_EQ("The storage of locks are not enough\nThe storage of stocks are not enough\n", _t_LockSockStorageNotEnough.soldProduct(71, 81, 3));
	EXPECT_EQ("Locks sold:70\nStocks sold:80\nBarrels sold:9\n", _t_LockSockStorageNotEnough.soldProduct(70, 80, 9));
	EXPECT_EQ("The storage of locks are not enough\nThe storage of stocks are not enough\n", _t_LockSockStorageNotEnough.soldProduct(3, 21, 1));
	EXPECT_EQ("The storage of locks are not enough\nThe storage of stocks are not enough\n", _t_LockSockStorageNotEnough.soldProduct(32, 10, 1));
}
//	Path 8 : 19 - 23 - 24 - 26 - 27 - 29 - 30 - 32 - 33
//	Status : Lock, Stock, Barrel's storage are all not enough.
TEST_F(ComissionTest, Test26_Function_SoldProduct_PathTesting_Path8) {
	Comission _t_AllProductStorageNotEnough;

	EXPECT_EQ("The storage of locks are not enough\nThe storage of stocks are not enough\nThe storage of barrels are not enough\n", _t_AllProductStorageNotEnough.soldProduct(71, 81, 91));
	EXPECT_EQ("Locks sold:70\nStocks sold:80\nBarrels sold:90\n", _t_AllProductStorageNotEnough.soldProduct(70, 80, 90));
	EXPECT_EQ("The storage of locks are not enough\nThe storage of stocks are not enough\nThe storage of barrels are not enough\n", _t_AllProductStorageNotEnough.soldProduct(30, 21, 1));
	EXPECT_EQ("The storage of locks are not enough\nThe storage of stocks are not enough\nThe storage of barrels are not enough\n", _t_AllProductStorageNotEnough.soldProduct(3, 10, 10));
}

