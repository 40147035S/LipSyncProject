#include "NextDateTest.h"
#include <sstream>

NextDateTest::NextDateTest() {
	std::cout << "\t[NextDateTest] Constructor..." << std::endl;

}

NextDateTest::~NextDateTest() {
	std::cout << "\t[NextDateTest] Deconstructor..." << std::endl;

}

void NextDateTest::SetUp() {
	std::cout << "\t[NextDateTest] Set Up..." << std::endl;

}

void NextDateTest::TearDown() {
	std::cout << "\t[NextDateTest] Tear Down..." << std::endl;

}

TEST_F(NextDateTest, Test01_validateFunctionNextDate_WeakRobustECTesting) {
	Date _t_WR1 = Date(1912, 6, 15);
	Date _t_WR2 = Date(1912, 6, -1);
	Date _t_WR3 = Date(1912, 6, 32);
	Date _t_WR4 = Date(1912, -1, 15);
	Date _t_WR5 = Date(1912, 13, 15);
	Date _t_WR6 = Date(1811, 6, 15);
	Date _t_WR7 = Date(2013, 6, 15);

	EXPECT_EQ("1912/6/16", _t_WR1.NextDate());
	EXPECT_EQ("Value of day not in the range 1..31", _t_WR2.NextDate());
	EXPECT_EQ("Value of day not in the range 1..31", _t_WR3.NextDate());
	EXPECT_EQ("Value of month not in the range 1..12", _t_WR4.NextDate());
	EXPECT_EQ("Value of month not in the range 1..12", _t_WR5.NextDate());
	EXPECT_EQ("Value of year not in the range 1812..2012", _t_WR6.NextDate());
	EXPECT_EQ("Value of year not in the range 1812..2012", _t_WR7.NextDate());
}

TEST_F(NextDateTest, Test02_validateFunctionNextDate_StrongRobustECTesting) {
	Date _t_SR1 = Date(1912, -1, 15);
	Date _t_SR2 = Date(1912, 6, -1);
	Date _t_SR3 = Date(1811, 6, 15);
	Date _t_SR4 = Date(1912, -1, -1);
	Date _t_SR5 = Date(1811, 6, -1);
	Date _t_SR6 = Date(1811, -1, 15);
	Date _t_SR7 = Date(1811, -1, -1);

	EXPECT_EQ("Value of month not in the range 1..12", _t_SR1.NextDate() );
	EXPECT_EQ("Value of day not in the range 1..31", _t_SR2.NextDate());
	EXPECT_EQ("Value of year not in the range 1812..2012", _t_SR3.NextDate());
	EXPECT_EQ("Value of month not in the range 1..12\nValue of day not in the range 1..31", _t_SR4.NextDate());
	EXPECT_EQ("Value of day not in the range 1..31\nValue of year not in the range 1812..2012", _t_SR5.NextDate());
	EXPECT_EQ("Value of month not in the range 1..12\nValue of year not in the range 1812..2012", _t_SR6.NextDate());
	EXPECT_EQ("Value of month not in the range 1..12\nValue of day not in the range 1..31\nValue of year not in the range 1812..2012", _t_SR7.NextDate());
}

TEST_F(NextDateTest, Test03_validateFunctionNextDate_WeakNormalECTesting) {
	Date _t_WN1 = Date(2000, 7, 14);
	Date _t_WN2 = Date(2000, 7, 31);
	Date _t_WN3 = Date(2000, 6, 31);
	Date _t_WN4 = Date(2000, 6, 30);
	Date _t_WN5 = Date(2000, 2, 30);
	Date _t_WN6 = Date(2000, 2, 29);
	Date _t_WN7 = Date(1900, 2, 29);
	Date _t_WN8 = Date(2000, 12, 31);

	EXPECT_EQ("2000/7/15", _t_WN1.NextDate());
	EXPECT_EQ("2000/8/1", _t_WN2.NextDate());
	EXPECT_EQ("impossible date", _t_WN3.NextDate());
	EXPECT_EQ("2000/7/1", _t_WN4.NextDate());
	EXPECT_EQ("impossible date", _t_WN5.NextDate());
	EXPECT_EQ("2000/3/1", _t_WN6.NextDate());
	EXPECT_EQ("impossible date", _t_WN7.NextDate());
	EXPECT_EQ("2001/1/1", _t_WN8.NextDate());
}

TEST_F(NextDateTest, Test04_YearTest_January1st_BoundaryValueTesting) {
	//	min-
	Date _t_BV1 = Date(1811, 1, 1);
	//	min
	Date _t_BV2 = Date(1812, 1, 1);
	//	min+
	Date _t_BV3 = Date(1813, 1, 1);
	//	max-
	Date _t_BV4 = Date(2011, 1, 1);
	//	max
	Date _t_BV5 = Date(2012, 1, 1);
	//	max+
	Date _t_BV6 = Date(2013, 1, 1);

	EXPECT_EQ("Value of year not in the range 1812..2012", _t_BV1.NextDate());
	EXPECT_EQ("1812/1/2", _t_BV2.NextDate());
	EXPECT_EQ("1813/1/2", _t_BV3.NextDate());
	EXPECT_EQ("2011/1/2", _t_BV4.NextDate());
	EXPECT_EQ("2012/1/2", _t_BV5.NextDate());
	EXPECT_EQ("Value of year not in the range 1812..2012", _t_BV6.NextDate());
}

TEST_F(NextDateTest, Test05_YearTest_February28th_BoundaryValueTesting) {
	//	min-
	Date _t_BV1 = Date(1811, 2, 28);
	//	min
	Date _t_BV2 = Date(1812, 2, 28);
	//	min+
	Date _t_BV3 = Date(1813, 2, 28);
	//	max-
	Date _t_BV4 = Date(2011, 2, 28);
	//	max
	Date _t_BV5 = Date(2012, 2, 28);
	//	max+
	Date _t_BV6 = Date(2013, 2, 28);

	EXPECT_EQ("Value of year not in the range 1812..2012", _t_BV1.NextDate());
	EXPECT_EQ("1812/2/29", _t_BV2.NextDate());
	EXPECT_EQ("1813/3/1", _t_BV3.NextDate());
	EXPECT_EQ("2011/3/1", _t_BV4.NextDate());
	EXPECT_EQ("2012/2/29", _t_BV5.NextDate());
	EXPECT_EQ("Value of year not in the range 1812..2012", _t_BV6.NextDate());

}

TEST_F(NextDateTest, Test06_YearTest_February29th_BoundaryValueTesting) {
	//	min-
	Date _t_BV1 = Date(1811, 2, 29);
	//	min
	Date _t_BV2 = Date(1812, 2, 29);
	//	min+
	Date _t_BV3 = Date(1813, 2, 29);
	//	max-
	Date _t_BV4 = Date(2011, 2, 29);
	//	max
	Date _t_BV5 = Date(2012, 2, 29);
	//	max+
	Date _t_BV6 = Date(2013, 2, 29);

	EXPECT_EQ("Value of year not in the range 1812..2012", _t_BV1.NextDate());
	EXPECT_EQ("1812/3/1", _t_BV2.NextDate());
	EXPECT_EQ("impossible date", _t_BV3.NextDate());
	EXPECT_EQ("impossible date", _t_BV4.NextDate());
	EXPECT_EQ("2012/3/1", _t_BV5.NextDate());
	EXPECT_EQ("Value of year not in the range 1812..2012", _t_BV6.NextDate());

}

TEST_F(NextDateTest, Test07_YearTest_December31th_BoundaryValueTesting) {
	//	min-
	Date _t_BV1 = Date(1811, 12, 31);
	//	min
	Date _t_BV2 = Date(1812, 12, 31);
	//	min+
	Date _t_BV3 = Date(1813, 12, 31);
	//	max-
	Date _t_BV4 = Date(2011, 12, 31);
	//	max
	Date _t_BV5 = Date(2012, 12, 31);
	//	max+
	Date _t_BV6 = Date(2013, 12, 31);

	EXPECT_EQ("Value of year not in the range 1812..2012", _t_BV1.NextDate());
	EXPECT_EQ("1813/1/1", _t_BV2.NextDate());
	EXPECT_EQ("1814/1/1", _t_BV3.NextDate());
	EXPECT_EQ("2012/1/1", _t_BV4.NextDate());
	EXPECT_EQ("2013/1/1", _t_BV5.NextDate());
	EXPECT_EQ("Value of year not in the range 1812..2012", _t_BV6.NextDate());
}

TEST_F(NextDateTest, Test08_YearTest_RandomDate_BoundaryValueTesting) {
	// Absolute Legal Date
	int month = rand() % 12 + 1;
	int date = rand() % 28 + 1;

	std::stringstream sMonth, sDate;

	//	min-
	Date _t_BV1 = Date(1811, month, date);
	//	min
	Date _t_BV2 = Date(1812, month, date);
	//	min+
	Date _t_BV3 = Date(1813, month, date);
	//	max-
	Date _t_BV4 = Date(2011, month, date);
	//	max
	Date _t_BV5 = Date(2012, month, date);
	//	max+
	Date _t_BV6 = Date(2013, month, date);

	int tomorrowDate = date + 1;
	sMonth << month;
	sDate << tomorrowDate;	

	EXPECT_EQ("Value of year not in the range 1812..2012", _t_BV1.NextDate());
	EXPECT_EQ("1812/" + sMonth.str() + "/" + sDate.str(), _t_BV2.NextDate() );
	EXPECT_EQ("1813/" + sMonth.str() + "/" + sDate.str(), _t_BV3.NextDate() );
	EXPECT_EQ("2011/" + sMonth.str() + "/" + sDate.str(), _t_BV4.NextDate() );
	EXPECT_EQ("2012/" + sMonth.str() + "/" + sDate.str(), _t_BV5.NextDate() );
	EXPECT_EQ("Value of year not in the range 1812..2012", _t_BV6.NextDate() );

}

TEST_F(NextDateTest, Test09_YearTest_ImpossibleDate_BoundaryValueTesting) {
	//	min-
	Date _t_BV1 = Date(1811, 6, 31);
	//	min
	Date _t_BV2 = Date(1812, 6, 31);
	//	min+
	Date _t_BV3 = Date(1813, 6, 31);
	//	max-
	Date _t_BV4 = Date(2011, 6, 31);
	//	max
	Date _t_BV5 = Date(2012, 6, 31);
	//	max+
	Date _t_BV6 = Date(2013, 6, 31);

	EXPECT_EQ("Value of year not in the range 1812..2012", _t_BV1.NextDate());
	EXPECT_EQ("impossible date", _t_BV2.NextDate());
	EXPECT_EQ("impossible date", _t_BV3.NextDate());
	EXPECT_EQ("impossible date", _t_BV4.NextDate());
	EXPECT_EQ("impossible date", _t_BV5.NextDate());
	EXPECT_EQ("Value of year not in the range 1812..2012", _t_BV6.NextDate());
}

TEST_F(NextDateTest, Test10_MonthTest_AverageYear_BoundaryValueTesting) {
	//	min-
	Date _t_BV1 = Date(1913, 0, 29);
	//	min
	Date _t_BV2 = Date(1913, 1, 29);
	//	min+
	Date _t_BV3 = Date(1913, 2, 29);
	//	max-
	Date _t_BV4 = Date(1913, 11, 29);
	//	max
	Date _t_BV5 = Date(1913, 12, 29);
	//	max+
	Date _t_BV6 = Date(1913, 13, 29);

	EXPECT_EQ("Value of month not in the range 1..12", _t_BV1.NextDate());
	EXPECT_EQ("1913/1/30", _t_BV2.NextDate());
	EXPECT_EQ("impossible date", _t_BV3.NextDate());
	EXPECT_EQ("1913/11/30", _t_BV4.NextDate());
	EXPECT_EQ("1913/12/30", _t_BV5.NextDate());
	EXPECT_EQ("Value of month not in the range 1..12", _t_BV6.NextDate());
}

TEST_F(NextDateTest, Test11_MonthTest_LeapYear_BoundaryValueTesting) {
	//	min-
	Date _t_BV1 = Date(1912, 0, 29);
	//	min
	Date _t_BV2 = Date(1912, 1, 29);
	//	min+
	Date _t_BV3 = Date(1912, 2, 29);
	//	max-
	Date _t_BV4 = Date(1912, 11, 29);
	//	max
	Date _t_BV5 = Date(1912, 12, 29);
	//	max+
	Date _t_BV6 = Date(1912, 13, 29);

	EXPECT_EQ("Value of month not in the range 1..12", _t_BV1.NextDate());
	EXPECT_EQ("1912/1/30", _t_BV2.NextDate());
	EXPECT_EQ("1912/3/1", _t_BV3.NextDate());
	EXPECT_EQ("1912/11/30", _t_BV4.NextDate());
	EXPECT_EQ("1912/12/30", _t_BV5.NextDate());
	EXPECT_EQ("Value of month not in the range 1..12", _t_BV6.NextDate());

}

TEST_F(NextDateTest, Test12_DayTest_January_BoundaryValueTesting) {
	//	min-
	Date _t_BV1 = Date(1912, 1, 0);
	//	min
	Date _t_BV2 = Date(1912, 1, 1);
	//	min+
	Date _t_BV3 = Date(1912, 1, 2);
	//	max-
	Date _t_BV4 = Date(1912, 1, 30);
	//	max
	Date _t_BV5 = Date(1912, 1, 31);
	//	max+
	Date _t_BV6 = Date(1912, 1, 32);

	EXPECT_EQ("Value of day not in the range 1..31", _t_BV1.NextDate());
	EXPECT_EQ("1912/1/2", _t_BV2.NextDate());
	EXPECT_EQ("1912/1/3", _t_BV3.NextDate());
	EXPECT_EQ("1912/1/31", _t_BV4.NextDate());
	EXPECT_EQ("1912/2/1", _t_BV5.NextDate());
	EXPECT_EQ("Value of day not in the range 1..31", _t_BV6.NextDate());
}

TEST_F(NextDateTest, Test13_DayTest_April_BoundaryValueTesting) {
	//	min-
	Date _t_BV1 = Date(1912, 4, 0);
	//	min
	Date _t_BV2 = Date(1912, 4, 1);
	//	min+
	Date _t_BV3 = Date(1912, 4, 2);
	//	max-
	Date _t_BV4 = Date(1912, 4, 30);
	//	max
	Date _t_BV5 = Date(1912, 4, 31);
	//	max+
	Date _t_BV6 = Date(1912, 4, 32);

	EXPECT_EQ("Value of day not in the range 1..31", _t_BV1.NextDate());
	EXPECT_EQ("1912/4/2", _t_BV2.NextDate());
	EXPECT_EQ("1912/4/3", _t_BV3.NextDate());
	EXPECT_EQ("1912/5/1", _t_BV4.NextDate());
	EXPECT_EQ("impossible date", _t_BV5.NextDate());
	EXPECT_EQ("Value of day not in the range 1..31", _t_BV6.NextDate());
}

TEST_F(NextDateTest, Test14_DayTest_February_BoundaryValueTesting) {
	//	min-
	Date _t_BV1 = Date(1912, 2, 0);
	//	min
	Date _t_BV2 = Date(1912, 2, 1);
	//	min+
	Date _t_BV3 = Date(1912, 2, 2);
	//	max-
	Date _t_BV4 = Date(1912, 2, 30);
	//	max
	Date _t_BV5 = Date(1912, 2, 31);
	//	max+
	Date _t_BV6 = Date(1912, 2, 32);

	EXPECT_EQ("Value of day not in the range 1..31", _t_BV1.NextDate());
	EXPECT_EQ("1912/2/2", _t_BV2.NextDate());
	EXPECT_EQ("1912/2/3", _t_BV3.NextDate());
	EXPECT_EQ("impossible date", _t_BV4.NextDate());
	EXPECT_EQ("impossible date", _t_BV5.NextDate());
	EXPECT_EQ("Value of day not in the range 1..31", _t_BV6.NextDate());
}

TEST_F(NextDateTest, Test15_DayTest_December_BoundaryValueTesting) {
	//	min-
	Date _t_BV1 = Date(1912, 12, 0);
	//	min
	Date _t_BV2 = Date(1912, 12, 1);
	//	min+
	Date _t_BV3 = Date(1912, 12, 2);
	//	max-
	Date _t_BV4 = Date(1912, 12, 30);
	//	max
	Date _t_BV5 = Date(1912, 12, 31);
	//	max+
	Date _t_BV6 = Date(1912, 12, 32);

	EXPECT_EQ("Value of day not in the range 1..31", _t_BV1.NextDate());
	EXPECT_EQ("1912/12/2", _t_BV2.NextDate());
	EXPECT_EQ("1912/12/3", _t_BV3.NextDate());
	EXPECT_EQ("1912/12/31", _t_BV4.NextDate());
	EXPECT_EQ("1913/1/1", _t_BV5.NextDate());
	EXPECT_EQ("Value of day not in the range 1..31", _t_BV6.NextDate());
}

//	TEST if the 3 variables, Year, Month, Day, are all in the predefined range. 
TEST_F(NextDateTest, Test16_Function_isLegalDate_PathTesting_Path01) {
	//	Status ; legalMonth, legalDay, legalYear

	Date _t_legalDate01(1912, 6, 31);
	EXPECT_EQ("impossible date", _t_legalDate01.isLegalDate());
	Date _t_legalDate02(2012, 2, 31);
	EXPECT_EQ("impossible date", _t_legalDate02.isLegalDate());
	Date _t_legalDate03(2005, 2, 29);
	EXPECT_EQ("impossible date", _t_legalDate03.isLegalDate());
	Date _t_legalDate04(2004, 2, 29);
	EXPECT_EQ("", _t_legalDate04.isLegalDate());
	Date _t_legalDate05(1900, 2, 29);
	EXPECT_EQ("impossible date", _t_legalDate05.isLegalDate());
	Date _t_legalDate06(2000, 2, 29);
	EXPECT_EQ("", _t_legalDate06.isLegalDate());
}

TEST_F(NextDateTest, Test17_Function_isLegalDate_PathTesting_Path02) {
	//	Status : legalMonth, legalDay, illegalYear

	Date _t_illegalYear01(1811, 7, 1);
	EXPECT_EQ("Value of year not in the range 1812..2012", _t_illegalYear01.isLegalDate());
	Date _t_illegalYear02(2013, 12, 31);
	EXPECT_EQ("Value of year not in the range 1812..2012", _t_illegalYear02.isLegalDate());
}

TEST_F(NextDateTest, Test18_Function_isLegalDate_PathTesting_Path03) {
	//	Status : legalMonth, illegalDay, legalYear

	Date _t_illegalDay01(1812, 7, 32);
	EXPECT_EQ("Value of day not in the range 1..31", _t_illegalDay01.isLegalDate());
	Date _t_illegalDay02(2012, 11, 0);
	EXPECT_EQ("Value of day not in the range 1..31", _t_illegalDay02.isLegalDate());
}

TEST_F(NextDateTest, Test19_Function_isLegalDate_PathTesting_Path04) {
	//	Status : legalMonth, illegalDay, illegalYear

	Date _t_illegalDayYear01(1800, 7, 32);
	EXPECT_EQ("Value of day not in the range 1..31\nValue of year not in the range 1812..2012", _t_illegalDayYear01.isLegalDate());
	Date _t_illegalDayYear02(2023, 12, 0);
	EXPECT_EQ("Value of day not in the range 1..31\nValue of year not in the range 1812..2012", _t_illegalDayYear02.isLegalDate());
}

TEST_F(NextDateTest, Test20_Function_isLegalDate_PathTesting_Path05) {
	//	Status : illegalMonth, legalDay, legalYear

	Date _t_illegalMonth01(1813, -1, 31);
	EXPECT_EQ("Value of month not in the range 1..12", _t_illegalMonth01.isLegalDate());
	Date _t_illegalMonth02(2000, 13, 30);
	EXPECT_EQ("Value of month not in the range 1..12", _t_illegalMonth02.isLegalDate());
}

TEST_F(NextDateTest, Test21_Function_isLegalDate_PathTesting_Path06) {
	//	Status : illegalMonth, legalDay, illegalYear

	Date _t_illegalMonthYear01(1810, -1, 31);
	EXPECT_EQ("Value of month not in the range 1..12\nValue of year not in the range 1812..2012", _t_illegalMonthYear01.isLegalDate());
	Date _t_illegalMonthYear02(2020, 13, 30);
	EXPECT_EQ("Value of month not in the range 1..12\nValue of year not in the range 1812..2012", _t_illegalMonthYear02.isLegalDate());
}

TEST_F(NextDateTest, Test22_Function_isLegalDate_PathTesting_Path07) {
	//	Status : illegalMonth, illegalDay, legalYear

	Date _t_illegalMonthDay01(1812, -1, -2);
	EXPECT_EQ("Value of month not in the range 1..12\nValue of day not in the range 1..31", _t_illegalMonthDay01.isLegalDate());
	Date _t_illegalMonthDay02(2000, 13, 32);
	EXPECT_EQ("Value of month not in the range 1..12\nValue of day not in the range 1..31", _t_illegalMonthDay02.isLegalDate());
}

TEST_F(NextDateTest, Test23_Function_isLegalDate_PathTesting_Path08) {
	//	Status : illegalMonth, illegalDay, illegalYear

	Date _t_illegalMonthDayYear01(1810, -1, 32);
	EXPECT_EQ("Value of month not in the range 1..12\nValue of day not in the range 1..31\nValue of year not in the range 1812..2012", _t_illegalMonthDayYear01.isLegalDate());
	Date _t_illegalMonthDayYear02(2020, 13, -1);
	EXPECT_EQ("Value of month not in the range 1..12\nValue of day not in the range 1..31\nValue of year not in the range 1812..2012", _t_illegalMonthDayYear02.isLegalDate());

}

//	In the premise of the 3 variables, Year, Month, Day, are all in the legal range.
//	Check the possibility of the input date.
TEST_F(NextDateTest, Test24_Function_isLegalDate_PathTesting) {
	//	In the premise of the legal date, to check the date's possibility.

	//	Path 1 : 32 - 33 - 49 : Month with 31 days, 1 <= Day <= 31
	//	Possible Date
	Date _t_January1st2001(2001, 1, 1);
	EXPECT_EQ("", _t_January1st2001.isLegalDate() );
	Date _t_March2nd2000(2000, 3, 2);
	EXPECT_EQ("", _t_March2nd2000.isLegalDate());
	Date _t_May3rd1999(1999, 5, 3);
	EXPECT_EQ("", _t_May3rd1999.isLegalDate());
	Date _t_July4th1998(1998, 7, 4);
	EXPECT_EQ("", _t_July4th1998.isLegalDate());
	Date _t_August5th1997(1997, 8, 5);
	EXPECT_EQ("", _t_August5th1997.isLegalDate());
	Date _t_October6th1996(1996, 10, 6);
	EXPECT_EQ("", _t_October6th1996.isLegalDate());
	Date _t_December7th1995(1995, 12, 7);
	EXPECT_EQ("", _t_December7th1995.isLegalDate());
	//	Path 2 : 32 - 35 - 36 - 49 : Month with 30 days, 1 <= Day <= 30
	//	Possible Date
	Date _t_April30th1919(1919, 4, 30);
	EXPECT_EQ("", _t_April30th1919.isLegalDate());
	Date _t_June29th1920(1920, 6, 29);
	EXPECT_EQ("", _t_June29th1920.isLegalDate());
	Date _t_September28th1921(1921, 9, 28);
	EXPECT_EQ("", _t_September28th1921.isLegalDate());
	Date _t_November27th1922(1922, 11, 27);
	EXPECT_EQ("", _t_November27th1922.isLegalDate());
	//Path 3 : 32 - 35 - 36 - 37 : Month with 30 days, Day == 31
	//	Impossible Date
	Date _t_April31th1919(1919, 4, 31);
	EXPECT_EQ("impossible date", _t_April31th1919.isLegalDate());
	Date _t_June31th1920(1920, 6, 31);
	EXPECT_EQ("impossible date", _t_June31th1920.isLegalDate());
	Date _t_September31th1921(1921, 9, 31);
	EXPECT_EQ("impossible date", _t_September31th1921.isLegalDate());
	Date _t_November31th1922(1922, 11, 31);
	EXPECT_EQ("impossible date", _t_November31th1922.isLegalDate());
	//	Path 4 : 32 - 39 - 40 - 41 : Month == February, Day == 30 || Day == 31
	//	Impossible Date
	Date _t_February30th2005(2005, 2, 30);
	EXPECT_EQ("impossible date", _t_February30th2005.isLegalDate());
	Date _t_February31th2005(2005, 2, 31);
	EXPECT_EQ("impossible date", _t_February31th2005.isLegalDate());
	Date _t_February30th2008(2008, 2, 30);
	EXPECT_EQ("impossible date", _t_February30th2008.isLegalDate());
	Date _t_February31th2008(2008, 2, 31);
	EXPECT_EQ("impossible date", _t_February31th2008.isLegalDate());
	//	Path 5 : 32 - 39 - 40 - 42 - 49 : Month == February, 1 <= Day <= 28
	//	Possible Date
	Date _t_February28th2005(2005, 2, 28);
	EXPECT_EQ("", _t_February28th2005.isLegalDate());
	Date _t_February27th2005(2005, 2, 27);
	EXPECT_EQ("", _t_February27th2005.isLegalDate());
	Date _t_February1st2008(2008, 2, 1);
	EXPECT_EQ("", _t_February1st2008.isLegalDate());
	Date _t_February2nd2008(2008, 2, 2);
	EXPECT_EQ("", _t_February2nd2008.isLegalDate());
	//	Path 6 : 32 - 39 - 40 - 42 - 43 - 49 : Month == February, Day == 29, Leap Year
	//	Possible Date
	//	% 4 == 0 : Leap Year
	Date _t_February29th2012(2012, 2, 29);
	EXPECT_EQ("", _t_February29th2012.isLegalDate());
	//	% 4 == 0 : Leap Year
	Date _t_February29th2008(2008, 2, 29);
	EXPECT_EQ("", _t_February29th2008.isLegalDate());
	//	% 4 == 0 , % 100 == 0, %400 == 0 : Leap Year
	Date _t_February29th2000(2000, 2, 29);
	EXPECT_EQ("", _t_February29th2000.isLegalDate());

	//Path 7 : 32 - 39 - 40 - 42 - 43 - 44 : Month == February, Day == 29, None Leap Year
	//	Impossible Date	
	//	%4 != 0 : None Leap Year
	Date _t_February29th1903(1903, 2, 29);
	EXPECT_EQ("impossible date", _t_February29th1903.isLegalDate());
	//	%4 != 0 : None Leap Year
	Date _t_February29th2007(2007, 2, 29);
	EXPECT_EQ("impossible date", _t_February29th2007.isLegalDate());
	//	%4 == 0, %100 == 0 : None Leap Year
	Date _t_February29th1900(1900, 2, 29);
	EXPECT_EQ("impossible date", _t_February29th1900.isLegalDate());
}

//	Path 1 : 5 - 6
//	None Legal Date (with the premise of the legal Year, Month, and Day)
TEST_F(NextDateTest, Test25_Function_NextDate_PathTesting_Path01) {
	//	Year % 4 != 0 : None Leap Year
	Date _t_February29th1901(1901, 2, 29);
	EXPECT_EQ("impossible date", _t_February29th1901.NextDate());
	//	Year % 4 == 0 && Year % 100 != 0 : None Leap Year
	Date _t_February29th1900(1900, 2, 29);
	EXPECT_EQ("impossible date", _t_February29th1900.NextDate());
	Date _t_February30th1918(1918, 2, 30);
	EXPECT_EQ("impossible date", _t_February30th1918.NextDate());
	Date _t_April31th1919(1919, 4, 31);
	EXPECT_EQ("impossible date", _t_April31th1919.NextDate());
	Date _t_June31th1920(1920, 6, 31);
	EXPECT_EQ("impossible date", _t_June31th1920.NextDate());
	Date _t_September31th1921(1921, 9, 31);
	EXPECT_EQ("impossible date", _t_September31th1921.NextDate());
	Date _t_November31th1922(1922, 11, 31);
	EXPECT_EQ("impossible date", _t_November31th1922.NextDate());
}
//	Path 1 : 5 - 6
//	None Legal Date (with the premise of the illegal Year, Month, and Day)
TEST_F(NextDateTest, Test26_Function_NextDate_PathTesting_Path01) {
	//	illegal Day
	Date _t_illegalDate01(1901, 2, 32);
	EXPECT_EQ("Value of day not in the range 1..31", _t_illegalDate01.NextDate());
	//	illegal Month
	Date _t_illegalDate02(1900, 13, 29);
	EXPECT_EQ("Value of month not in the range 1..12", _t_illegalDate02.NextDate());
	//	illegal Year
	Date _t_illegalDate03(1800, 2, 30);
	EXPECT_EQ("Value of year not in the range 1812..2012", _t_illegalDate03.NextDate());
	//	illegal Day, Month
	Date _t_illegalDate04(1919, 14, 0);
	EXPECT_EQ("Value of month not in the range 1..12\nValue of day not in the range 1..31", _t_illegalDate04.NextDate());
	//	illegal Day, Year
	Date _t_illegalDate05(2020, 6, 50);
	EXPECT_EQ("Value of day not in the range 1..31\nValue of year not in the range 1812..2012", _t_illegalDate05.NextDate());
	//	illegal Month, Year
	Date _t_illegalDate06(2076, 18, 31);
	EXPECT_EQ("Value of month not in the range 1..12\nValue of year not in the range 1812..2012", _t_illegalDate06.NextDate());
	//	illegal Day, Month, Year
	Date _t_illegalDate07(1716, -1, -300);
	EXPECT_EQ("Value of month not in the range 1..12\nValue of day not in the range 1..31\nValue of year not in the range 1812..2012", _t_illegalDate07.NextDate());
}
//	Path 2 : 5 - 11 - 13 - 14 - 15 - 63
//	Legal Date
//	Month : { 1, 3, 5, 7, 8, 10 }, 1 <= Day <= 30 
//	No Increment of Month / Year, Increment of Day
TEST_F(NextDateTest, Test27_Function_NextDate_PathTesting_Path02) {
	Date _t_January1st2000(2000, 1, 1);
	EXPECT_EQ("2000/1/2", _t_January1st2000.NextDate());
	Date _t_March30th2000(2000, 3, 30);
	EXPECT_EQ("2000/3/31", _t_March30th2000.NextDate());
	Date _t_May15th2000(2000, 5, 15);
	EXPECT_EQ("2000/5/16", _t_May15th2000.NextDate());
	Date _t_July17th2000(2000, 7, 17);
	EXPECT_EQ("2000/7/18", _t_July17th2000.NextDate());
	Date _t_August29th2000(2000, 8, 29);
	EXPECT_EQ("2000/8/30", _t_August29th2000.NextDate());
	Date _t_October10th2000(2000, 10, 10);
	EXPECT_EQ("2000/10/11", _t_October10th2000.NextDate());
}

//	Path 3 : 5 - 11 - 13 - 14 - 16 - 17 - 63
//	Legal Date
//	Month : { 1, 3, 5, 7, 8, 10 }, Day == 31
//	No Increment of Year, Increment of Month, Reset Day to 1
TEST_F(NextDateTest, Test28_Function_NextDate_PathTesting_Path03) {
	Date _t_January31th2001(2001, 1, 31);
	EXPECT_EQ("2001/2/1", _t_January31th2001.NextDate());
	Date _t_March31th2001(2001, 3, 31);
	EXPECT_EQ("2001/4/1", _t_March31th2001.NextDate());
	Date _t_May31th2001(2001, 5, 31);
	EXPECT_EQ("2001/6/1", _t_May31th2001.NextDate());
	Date _t_July31th2001(2001, 7, 31);
	EXPECT_EQ("2001/8/1", _t_July31th2001.NextDate());
	Date _t_August31th2001(2001, 8, 31);
	EXPECT_EQ("2001/9/1", _t_August31th2001.NextDate());
	Date _t_October31th2001(2001, 10, 31);
	EXPECT_EQ("2001/11/1", _t_October31th2001.NextDate());
}
//	Path 4 : 5 - 11 - 22 - 23 - 24 - 63
//	Legal Date
//	Month : { 12 }, 1 <= Day <= 30
//	No Increment of Year / Month, Increment of Day
TEST_F(NextDateTest, Test29_Function_NextDate_PathTesting_Path04) {
	Date _t_December1st2001(2001, 12, 1);
	EXPECT_EQ("2001/12/2", _t_December1st2001.NextDate());
	Date _t_December2nd2001(2001, 12, 2);
	EXPECT_EQ("2001/12/3", _t_December2nd2001.NextDate());
	Date _t_December29th2001(2001, 12, 29);
	EXPECT_EQ("2001/12/30", _t_December29th2001.NextDate());
	Date _t_December30th2001(2001, 12, 30);
	EXPECT_EQ("2001/12/31", _t_December30th2001.NextDate());
}

//	Path 5 : 5 - 11 - 22 - 23 - 25 - 26 - 63
//	Legal Date
//	Month : { 12 }, Day == 31
//	Increment of Year, Reset Month to 1, Reset Day to 1
TEST_F(NextDateTest, Test30_Function_NextDate_PathTesting_Path05) {
	Date _t_December31th2001(2001, 12, 31);
	EXPECT_EQ("2002/1/1", _t_December31th2001.NextDate());
	Date _t_December31th2002(2002, 12, 31);
	EXPECT_EQ("2003/1/1", _t_December31th2002.NextDate());
	Date _t_December31th2003(2003, 12, 31);
	EXPECT_EQ("2004/1/1", _t_December31th2003.NextDate());
	Date _t_December31th2004(2004, 12, 31);
	EXPECT_EQ("2005/1/1", _t_December31th2004.NextDate());
}
//	Path 6 : 5 - 11 - 32 - 33 - 34 - 63
//	Legal Date
//	Month : { 4, 6, 9, 11 }, 1 <= Day <= 29
//	No Increment of Year / Month, Increment of Day
TEST_F(NextDateTest, Test31_Function_NextDate_PathTesting_Path06) {
	Date _t_April1st2000(2000, 4, 1);
	EXPECT_EQ("2000/4/2", _t_April1st2000.NextDate());
	Date _t_June29th2000(2000, 6, 29);
	EXPECT_EQ("2000/6/30", _t_June29th2000.NextDate());
	Date _t_September15th2000(2000, 9, 15);
	EXPECT_EQ("2000/9/16", _t_September15th2000.NextDate());
	Date _t_November17th2000(2000, 11, 17);
	EXPECT_EQ("2000/11/18", _t_November17th2000.NextDate());
}
//	Path 7 : 5 - 11 - 32 - 33 - 35 - 36 - 63
//	Legal Date
//	Month : { 4, 6, 9, 11 }, Day == 30
//	No Increment of Year, Increment of Month, Reset Day to 1
TEST_F(NextDateTest, Test32_Function_NextDate_PathTesting_Path7) {
	Date _t_April30th2000(2000, 4, 30);
	EXPECT_EQ("2000/5/1", _t_April30th2000.NextDate());
	Date _t_June30th2000(2000, 6, 30);
	EXPECT_EQ("2000/7/1", _t_June30th2000.NextDate());
	Date _t_September30th2000(2000, 9, 30);
	EXPECT_EQ("2000/10/1", _t_September30th2000.NextDate());
	Date _t_November30th2000(2000, 11, 30);
	EXPECT_EQ("2000/12/1", _t_November30th2000.NextDate());
}
//	Path 8 : 5 - 11 - 41 - 42 - 43 - 63
//	Legal Date
//	Month = {2}, 1 <= Day <= 27
//	No Increment of Year / Month, Increment of Day
TEST_F(NextDateTest, Test33_Function_NextDate_PathTesting_Path8) {
	Date _t_February1st2003(2003, 2, 1);
	EXPECT_EQ("2003/2/2", _t_February1st2003.NextDate());
	Date _t_February2nd2003(2003, 2, 2);
	EXPECT_EQ("2003/2/3", _t_February2nd2003.NextDate());
	Date _t_February26th2003(2003, 2, 26);
	EXPECT_EQ("2003/2/27", _t_February26th2003.NextDate());
	Date _t_February27th2003(2003, 2, 27);
	EXPECT_EQ("2003/2/28", _t_February27th2003.NextDate());
}
//	Path 9 : 5 - 11 - 41 - 42 - 44 - 56 - 57 - 63
//	Legal Date
//	Month = {2},  Day == 29
//	Output : 3/1
TEST_F(NextDateTest, Test34_Function_NextDate_PathTesting_Path9) {
	Date _t_February29th2000(2000, 2, 29);
	EXPECT_EQ("2000/3/1", _t_February29th2000.NextDate());
	Date _t_February29th2004(2004, 2, 29);
	EXPECT_EQ("2004/3/1", _t_February29th2004.NextDate());
	Date _t_February29th2008(2008, 2, 29);
	EXPECT_EQ("2008/3/1", _t_February29th2008.NextDate());
	Date _t_February29th2012(2012, 2, 29);
	EXPECT_EQ("2012/3/1", _t_February29th2012.NextDate());
}
//Path 10 : 5 - 11 - 41 - 42 - 44 - 46 - 47 - 63
//	Legal Date
//	Month = {2},  Day == 28, Average Year
//	Output : 3/1
TEST_F(NextDateTest, Test33_Function_NextDate_PathTesting_Path10) {
	Date _t_February28th2001(2001, 2, 28);
	EXPECT_EQ("2001/3/1", _t_February28th2001.NextDate());
	Date _t_February28th2005(2005, 2, 28);
	EXPECT_EQ("2005/3/1", _t_February28th2005.NextDate());
	Date _t_February28th2009(2009, 2, 28);
	EXPECT_EQ("2009/3/1", _t_February28th2009.NextDate());
	Date _t_February28th2011(2011, 2, 28);
	EXPECT_EQ("2011/3/1", _t_February28th2011.NextDate());
}
//	Path 11 : 5 - 11 - 41 - 42 - 44 - 46 - 51 - 52 - 63
//	Legal Date
//	Month = {2},  Day == 28, Leap Year
//	Output : 2/29
TEST_F(NextDateTest, Test34_Function_NextDate_PathTesting_Path11) {
	Date _t_February28th2000(2000, 2, 28);
	EXPECT_EQ("2000/2/29", _t_February28th2000.NextDate());
	Date _t_February28th2004(2004, 2, 28);
	EXPECT_EQ("2004/2/29", _t_February28th2004.NextDate());
	Date _t_February28th2008(2008, 2, 28);
	EXPECT_EQ("2008/2/29", _t_February28th2008.NextDate());
	Date _t_February28th2012(2012, 2, 28);
	EXPECT_EQ("2012/2/29", _t_February28th2012.NextDate());
}


