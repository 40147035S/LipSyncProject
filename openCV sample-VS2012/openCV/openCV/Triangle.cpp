#include <math.h>
#include "Triangle.h"
#include <string>

Triangle::Triangle(int e1, int e2, int e3) 
:a(e1), b(e2), c(e3) {}

std::string Triangle::isLegal() const {
	//	6 rules to check if the Triangle Object is actually a triangle.
	//	c1 : 1 <= a <= 200
	//	c2 : 1 <= b <= 200
	//	c3 : 1 <= c <= 200
	//	c4 : a + b > c
	//	c5 : a + c > b
	//	c6 : b + c > a

	myRange edgeRange(1, 200);

	//	check Rule c1 :
	bool legalA = edgeRange.inRange(a);
	//	check Rule c2 :
	bool legalB = edgeRange.inRange(b);
	//	check Rule c3 :
	bool legalC = edgeRange.inRange(c);
	
	if (!legalA && !legalB && !legalC) 
		return "Value of a,b,c are not in the range of " + edgeRange.ShowRange();
	if (!legalA && !legalB && legalC)
		return "Value of a,b are not in the range of " + edgeRange.ShowRange();
	if (!legalA && legalB && !legalC)
		return "Value of a,c are not in the range of " + edgeRange.ShowRange();
	if (!legalA && legalB && legalC)
		return "Value of a is not in the range of " + edgeRange.ShowRange();
	if (legalA && !legalB && !legalC)
		return "Value of b,c are not in the range of " + edgeRange.ShowRange();
	if (legalA && !legalB && legalC)
		return "Value of b is not in the range of " + edgeRange.ShowRange();
	if (legalA && legalB && !legalC)
		return "Value of c is not in the range of " + edgeRange.ShowRange();
	if (legalA && legalB && legalC)
		return "";
}

std::string Triangle::TriangleType() const {
	std::string edgesAreLegal = isLegal();

	//	Check if the 3 edges are in the range.
	if (edgesAreLegal != "")
		return edgesAreLegal;
	//	Check if it's a triangle.
	//	check Rule c4:
	//	check Rule c5:
	//	check Rule c6:
	if (a + b <= c || a + c <= b || b + c <= a)
		return "Not A Triangle";

	if (a == b && a == c) 
		return "Equilateral";	

	else if (a != b && a != c && b != c) 
		return "Scalene";
	else
		return "Isosceles";
}

