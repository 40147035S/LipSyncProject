#include <stdio.h>
#include <string>

#include "Range.h"

class Triangle{
public :
	int a, b, c;

	Triangle() = default;
	//	The Triangle should give an exception when any of the edge is illegal. 
	Triangle(int e1, int e2, int e3);

public:
	///< Check if the Triangle Object is Legal.
	///< Output "" if it is; otherwise output error message.
	std::string isLegal() const;
	///< Check the type of the Triangle Object.
	///< Output the type of the Triangle Object.
	std::string TriangleType() const;
};