
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <string>
#include <highgui.h>
#include <cmath>
#include "cv.h"

#include "gtest/gtest.h"

#include "src/gtest.cc"
#include "src/gtest-death-test.cc"
#include "src/gtest-filepath.cc"
#include "src/gtest-port.cc"
#include "src/gtest-printers.cc"
#include "src/gtest-test-part.cc"
#include "src/gtest-typed-test.cc"

#include "TriangleTest.h"
#include "NextDateTest.h"

using namespace std;
using namespace cv;

int main(int argc, char** argv){
	testing::InitGoogleTest(&argc, argv);

	int R = RUN_ALL_TESTS();

	system("pause");
	
	Date d;
	d.today();
	std::cout << d.OutputDate() << std::endl;

	system("pause");

	return R;
}
