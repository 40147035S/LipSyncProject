#include "RangeTest.h"

RangeTest::RangeTest() {
	std::cout << "\t[RangeTest] Constructor..." << std::endl;
}

RangeTest:: ~RangeTest() {
	std::cout << "\t[RangeTest] Deconstructor" << std::endl;
}

void RangeTest::SetUp() {
	std::cout << "\t[RangeTest] Set Up..." << std::endl;
}

void RangeTest::TearDown() {
	std::cout << "\t[RangeTest] Tear Down..." << std::endl;
}

TEST_F(RangeTest, Test01_FloorLargerThanCeiling) {
	EXPECT_THROW(myRange(3, 1), std::out_of_range);
	EXPECT_THROW(myRange(10, -1), std::out_of_range);
	EXPECT_THROW(myRange(-2, -3), std::out_of_range);
}

TEST_F(RangeTest, Test02_FloorEqualToCeiling) {
	EXPECT_NO_THROW(myRange(3, 3));
	EXPECT_NO_THROW(myRange(-3, -3));
	EXPECT_NO_THROW(myRange(0, 0));
}

TEST_F(RangeTest, Test03_FloorSmallerThanCeiling) {
	EXPECT_NO_THROW(myRange(1, 3));
	EXPECT_NO_THROW(myRange(-1, 10));
	EXPECT_NO_THROW(myRange(-3, -2));
}

TEST_F(RangeTest, Test04_NumberIsInRange_BoundaryValueTesting) {
	myRange _range_1_4(1, 4);

	//	Boundary Value Testing :
	//	min, max
	EXPECT_EQ(true, _range_1_4.inRange(1));
	EXPECT_EQ(true, _range_1_4.inRange(4));
	//	min+, max-
	EXPECT_EQ(true, _range_1_4.inRange(2));
	EXPECT_EQ(true, _range_1_4.inRange(3));
	//	min-, max+
	EXPECT_EQ(false, _range_1_4.inRange(0));
	EXPECT_EQ(false, _range_1_4.inRange(5));
}

TEST_F(RangeTest, Test05_ShowTheRangeByString) {
	myRange _range_1_3(1, 3);

	EXPECT_EQ("1..3", _range_1_3.ShowRange());
}
