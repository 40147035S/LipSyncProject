#ifndef __RANGE_H__
#define __RANGE_H__

#include <string>

///< Class Range : store the range of data.
///< equals to [1, 10], if floor == 1 && ceiling == 10
class myRange{
public:
	///< Constructor
	myRange() = default;
	///< Constructor with params 'lower' (floor), 'upper' (ceiling)
	myRange(int lower, int upper);
	///< Copy Constructor
	myRange(myRange &other)
		:floor(other.floor), ceiling(other.ceiling) {}

	///< Destructor
	~myRange() = default;

	///< The input number is in range.
	bool inRange(int number) const;
	///< Show the Range by string.
	std::string ShowRange() const;
private:
	int floor, ceiling;
};

#endif