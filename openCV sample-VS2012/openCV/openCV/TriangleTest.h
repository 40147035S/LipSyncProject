#ifndef __TRIANGLETEST_H__
#define __TRIANGLETEST_H__

#include "Triangle.h"
#include "gtest/gtest.h"

class TriangleTest : public ::testing::Test {
protected :
	///< Constructor
	TriangleTest();

	///< Destructor
	virtual ~TriangleTest();

	virtual void SetUp();

	virtual void TearDown();
};

#endif