#include "NextDate.h"
#include <time.h>
#include <sstream>

const myRange Date::yearRange = myRange(1812, 2012);
const myRange Date::monthRange = myRange(1, 12);
const myRange Date::dayRange = myRange(1, 31);

void Date::today() {
	time_t now = time(0);
	
	tm *ltm = localtime(&now);

	year = 1900 + ltm->tm_year;
	month = 1 + ltm->tm_mon;
	day = ltm->tm_mday;
}

std::string Date::isLegalDate() const {
	bool legalDay = dayRange.inRange(day);
	bool legalMonth = monthRange.inRange(month);
	bool legalYear = yearRange.inRange(year);

	std::string legalDate = "";

	if (!legalMonth) {
		if (legalDate != "")
			legalDate += "\n";
		legalDate += "Value of month not in the range ";
		legalDate += monthRange.ShowRange();
	}

	if (!legalDay) {
		if (legalDate != "")
			legalDate += "\n";
		legalDate += "Value of day not in the range ";
		legalDate += dayRange.ShowRange();
	}

	if (!legalYear) {
		if (legalDate != "")
			legalDate += "\n";
		legalDate += "Value of year not in the range ";
		legalDate += yearRange.ShowRange();
	}
	//	input isn't legal.
	if (legalDate != "")
		return legalDate;
	
	switch(month){
	case 1: case 3: case 5: case 7: case 8: case 10: case 12:
		break;
	case 4: case 6: case 9: case 11:
		if (day == 31)
			return "impossible date";
		break;
	case 2:
		if (day == 30 || day == 31)
			return "impossible date";
		else if (day == 29) {
			if ( !(year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)) )
				return "impossible date";
		}			
		break;
	}

	return legalDate;
}

std::string Date::OutputDate() const{
	std::stringstream syear, smonth, sday;
	syear << year;
	smonth << month;
	sday << day;

	return std::string(syear.str() + "/" + 
		               smonth.str() + "/" + 
					   sday.str()
					   );
}

std::string Date::NextDate(){
	std::string legalMessage = isLegalDate();

	//	Legal Date check.
	if (legalMessage != "")
		return legalMessage;	

	Date tomorrowDate(*this);

	//	Check the month & day combination.
	switch (month) {
		//	1 ~ 31
	case 1: case 3: case 5: case 7: case 8: case 10:
		if (day >= 1 && day <= 30)
			tomorrowDate.day += 1;		
		else if (day == 31) {
			tomorrowDate.day = 1;
			tomorrowDate.month += 1;
		}
		break;
		//	1 ~ 31, new year's eve : 01/01
	case 12:
		if (day >= 1 && day <= 30)
			tomorrowDate.day += 1;
		else if (day == 31) {
			tomorrowDate.day = 1;
			tomorrowDate.month = 1;
			tomorrowDate.year += 1;
		}
		break;
		//	1 ~ 30
	case 4: case 6: case 9: case 11:
		if (day >= 1 && day <= 29)
			tomorrowDate.day += 1;
		else if (day == 30){
			tomorrowDate.day = 1;
			tomorrowDate.month += 1;
		}
		break;
		//	average year : 1~28, leap year : 1~29
	case 2:
		if (day >= 1 && day <= 27)
			tomorrowDate.day += 1;
		else if (day == 28) {
			//	average year : 03/01
			if (!(year % 400 == 0 || (year % 4 == 0 && year % 100 != 0))) {
				tomorrowDate.month = 3;
				tomorrowDate.day = 1;
			}				
			//	leap year : 02/29
			else {
				tomorrowDate.month = 2;
				tomorrowDate.day = 29;				
			}				
		}
		else if (day == 29) {
			tomorrowDate.month = 3;
			tomorrowDate.day = 1;
		}
		break;
	}

	return tomorrowDate.OutputDate();

}