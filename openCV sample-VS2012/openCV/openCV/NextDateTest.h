#ifndef __NEXTDATETEST_H__
#define __NEXTDATETEST_H__

#include "NextDate.h"
#include "gtest/gtest.h"

class NextDateTest : public ::testing::Test{
protected:
	///< Constructor
	NextDateTest();

	///< Destructor
	~NextDateTest();

	virtual void SetUp();

	virtual void TearDown();
};

#endif