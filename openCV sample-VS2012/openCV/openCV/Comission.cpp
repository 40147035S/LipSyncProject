#include "Comission.h"
#include <sstream>

const float Comission::lockPrice = 45;
const float Comission::stockPrice = 30;
const float Comission::barrelPrice = 25;

const int Comission::lockStorage = 70; 
const int Comission::stockStorage = 80;
const int Comission::barrelStorage = 90;

std::string Comission::soldProduct(int locks, int stocks, int barrels) {
	std::string error_message = "";
	
	if (locks == -1) {
		std::stringstream sComission;
		sComission << ComissionValue();

		return "Comission is " + sComission.str() + "\n";
	}

	//	illegal input of locks / stocks / barrels
	if (locks < 0) 
		error_message += "Value of lock is not natural number.\n";
	if (stocks < 0)
		error_message += "Value of stock is not natural number.\n";
	if (barrels < 0)
		error_message += "Value of barrel is not natural number.\n";

	if (error_message != "")
		return error_message;

	//	number of locks larger than the maximum amount.
	if (locks + locksSold > lockStorage) 
		error_message += "The storage of locks are not enough\n";
	
	if (stocks + stocksSold > stockStorage) 
		error_message += "The storage of stocks are not enough\n";
	
	if (barrels + barrelsSold > barrelStorage) 
		error_message += "The storage of barrels are not enough\n";

	if (error_message != "")
		return error_message;

	locksSold += locks;
	stocksSold += stocks;
	barrelsSold += barrels;

	std::stringstream sLock, sStock, sBarrel;
	sLock << locksSold;
	sStock << stocksSold;
	sBarrel << barrelsSold;

	return "Locks sold:"+ sLock.str() + "\nStocks sold:" + sStock.str() + "\nBarrels sold:"+ sBarrel.str() + "\n" ;
}

float Comission::ProductValue() const{
	return lockPrice * locksSold + stockPrice * stocksSold + barrelPrice * barrelsSold;
}

float Comission::ComissionValue() const {
	//	The salesman must sell at least one lock, one stock, one barrel
	if (locksSold < 1 || stocksSold < 1 || barrelsSold < 1)
		throw std::exception("Insufficient Amount of Products.");

	int productValue = ProductValue();

	if (productValue <= 1000)
		return productValue * 0.1f;
	else if (productValue <= 1800) 
		return (productValue - 1000) * 0.15f + 1000 * 0.1f;
	else
		return (productValue - 1800) * 0.2f + 800 * 0.15f + 1000 * 0.1f;
}
