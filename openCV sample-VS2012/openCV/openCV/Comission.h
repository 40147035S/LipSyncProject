#ifndef __COMMISSION_H__
#define __COMMISSION_H__

#include <string>

class Comission{
public :
	static const float lockPrice;
	static const float stockPrice;
	static const float barrelPrice;

	static const int lockStorage;
	static const int stockStorage;
	static const int barrelStorage;
public :
	Comission()
		:locksSold(0), stocksSold(0), barrelsSold(0) {}
	Comission(int lock, int stock, int barrel)
		:locksSold(lock), stocksSold(stock), barrelsSold(barrel){}
public:
	///< could sold 3 kinds of product every loop.
	std::string soldProduct(int locks, int stocks, int barrels);
	///< Compute the whole value of the product that the have been sold.
	float ProductValue() const;
	///< Compute the Value of Comission.
	float ComissionValue() const;
private:
	//	Represents how much products you have sold.
	int locksSold, stocksSold, barrelsSold;
};

#endif