#ifndef __RANGETEST_H__
#define __RANGETEST_H__

#include "Range.h"
#include "gtest/gtest.h"

class RangeTest : public::testing::Test{
public:
	///< Constructor
	RangeTest();

	///< Deconstructor
	virtual ~RangeTest();

	virtual void SetUp();

	virtual void TearDown();
};

#endif